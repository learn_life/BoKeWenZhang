> #### 安装 git

- Ubuntu
```shell
sudo apt-get install git
```
- CentOS
```shell
sudo yum install git
```
- Windows
  Windows到下面网站下载git
```
https://git-for-windows.github.io/
```

> #### 安装后配置git

```shell
$ git config --global user.name "Your Name"
$ git config --global user.email "email@example.com"
```
Git是分布式版本控制系统，所以，每个机器上都必须设置你的名字和email地址
注意上面的命令使用了 `--global` 参数，表示这个机器上的所有git仓库都是使用该设置，也可以针对不同的仓库设置不同的指定不同的设置。

---
> #### 操作 git

- __创建git版本库__ 

  在需要建立git仓库的目录下 __`git init`__ 初始化git版本库。下面在`tmp/repository` 创建git版本库

```shell
$ git init
已初始化空的 Git 仓库于 /tmp/repository/.git/
```

- __将文件添加版本库__

  使用 __`git add filename`__ 命令将文件添加到版本库

```shell
$ echo 'hello git!' > file.txt
$ git add file.txt 
```

​		__`git add`__ 执行成功是没有内容返回的

- __查看版本库状态__ 

  使用__`git status`__ 查看版本库的状态。可以看到刚添加的文件


```shell
$ git status
位于分支 master

初始提交

要提交的变更：
  （使用 "git rm --cached <文件>..." 以取消暂存）

	新文件：   file.txt
```

- __将文件提交到版本库__

  在使用 __`git add`__ 添加文件后，还需使用 __`git commit `__ 提交文件

```shell
$ git commit -m 'add file.txt'
[master（根提交） c8a1565] add file.txt
 1 file changed, 1 insertion(+)
 create mode 100644 file.txt
```

​		注意：提交文件时带上 __`-m`__ 参数，其后接的内容是本次提交的说明。

> #### 版本控制

现在再向文件添加内容并提交

```shell
$ echo 'hello world!' >> file.txt
$ git add file.txt 
$ git commit -m 'hello world!'
[master 526779e] hello world!
 1 files changed, 1 insertions(+), 0 deletions(-)
```

- __查看git的历史记录__

  使用 __`git log`__ 查看历史记录， __`--pretty=oneline`__ 简略输出。commit后面接的是版本的 commit id

```shell
$ git log
commit 526779e1214510fdd5be6538d0d506bdd6e99380
Author: Your Name <you@example.com>
Date:   Sun Oct 22 22:43:56 2017 +0800

    hello world!

commit b2dff22d07bc4d16b07d029742ac422518cf3241
Author: Your Name <you@example.com>
Date:   Sun Oct 22 22:42:55 2017 +0800

    add file.txt
```

-  __版本回退__

  使用 __`git reset --hard HEAD^`__ 回退上一个版本，同理 __`HEAD^^`__ 表示回退上上个版本，回退N个版本可以使用 __`HEAD~N`__ 


```shell
$ git reset --hard HEAD^
HEAD is now at b2dff22 add file.txt
```

​		使用 __`git log`__ 查看，已经回退了一个版本

```shell
git log --pretty=oneline
d6cde4decc69aba75329fef5a939975688bf377d add file.txt
```

- __撤销回退__

  现在使用 `git log` 已查不到上一个版本的commit id了。可以使用 __`git reflog`__ 查看

```shell
$ git reflog
d6cde4d HEAD@{0}: HEAD^: updating HEAD
8cf6e00 HEAD@{1}: commit: hello world!
```

​		`git rese --hard` 也可以利用 `commit id` 回退

```shell
$ git reset --hard 8cf6e00
HEAD is now at 8cf6e00 hello world!
```

​		使用`git log` 查看

```shell
$ git log --pretty=oneline
8cf6e00d7024d3e904a8f8cf4d99256e2d920d9a hello world!
d6cde4decc69aba75329fef5a939975688bf377d add file.txt
```

