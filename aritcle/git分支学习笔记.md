####  git 分支学习记录

- **查看分支**


```shell
$ git branch			
* master				# *表示当前所在分支
```

- **创建分支** 
```shell
$ git branch other         # 创建名为other的分支
$ git checkout other	   # 切换分支
或
$ git checkout -b other2   # 创建名为other2的分支并切换至该分支
```

- **合并分支**

```shell
 $ git merge 要合并的分支名
```

  创建名为`other` 的分支，写入一行内容并合并分支。

```shell
$ git branch					# 查看分支
* master
  other
$ git log --pretty=oneline
052886c28aab25367e23c9d816f0ea0810cbb497 hello git!
$ git checkout other
切换到分支 'other'
$ echo 'other branch' >> file.txt 
$ git add file.txt 
$ git commit -m 'other branch'
[other 696e471] other branch
 1 file changed, 1 insertion(+)
$ git log --pretty=oneline
696e471ca08c6e20f1e786e4f1756b9cb716497f other branch
052886c28aab25367e23c9d816f0ea0810cbb497 hello git!
$ git checkout master; git merge other
```

- **删除分支** 

```
$ git branch -d 分支名
```

  ​