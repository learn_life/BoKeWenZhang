问题描述：

在ansible安装完毕后一般需要以SSH的方式连接到需要进行管理的目标主机，一开始遇到了如下问题：

120.76.25.191 | UNREACHABLE! => {
​    "changed": false, 
​    "msg": "Failed to connect to the host via ssh: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).\r\n", 
​    "unreachable": true
}
120.76.78.194 | UNREACHABLE! => {
​    "changed": false, 
​    "msg": "Failed to connect to the host via ssh: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).\r\n", 
​    "unreachable": true

问题原因很简单，没有在ansible管理节点（即安装ansible的节点）上添加目标节点（即需要管理的节点）的ssh认证信息。

解决步骤：

1：管理节点生成SSH-KEY

```
#ssh-keygen
```

成功后在~/.ssh/路径下将生成ssh密钥文件：id_rsa及id_rsa.pub

 

2：添加目标节点的SSH认证信息

```
#ssh-copy-id root@目标节点IP
```

这里root是在目标节点上登录的用户，@符号后面接目标节点IP即可，之后会提示输入目标节点root用户密码，输入即可。

添加认证信息后，目标节点主机的~/.ssh/目录下将会出现一个authorized_keys文件，里面包含了ansible管理节点的公钥信息，可以检查一下是否存在。

3：在确定目标主机的SSH认证信息都已正确添加且目标主机的~/.ssh/目录都存在管理节点的公钥信息后，再执行之前出错的ansible ping指令：

\#ansible -m ping all

120.76.25.191 | SUCCES对之前未连接的主机进行连结时报错如下：S => {
​    "changed": false, 
​    "ping": "pong"
}
120.76.78.194 | SUCCESS => {
​    "changed": false, 
​    "ping": "pong"
}

问题描述：

对之前未连接的主机进行连结时报错如下：

[root@puppet ~]# ansible webservers -m command -a 'ls ~' -k
SSH password: 
120.76.25.191 | FAILED | rc=0 >>
Using a SSH password instead of a key is not possible because Host Key checking is enabled and sshpass does not support this.  Please add this host's fingerprint to your known_hosts file to manage this host.

解决步骤：

修改ansible.cfg文件

vi /etc/ansible/ansible.cfg

找到以下行，让host_key_checking=False这行生效

\# uncomment this to disable SSH key host checking
host_key_checking = False