### ssh client配置文件

ssh客户端的配置文件是`~/.ssh/config` ，可以在该文件中写入ssh登录的参数，不用每次ssh登录都需要设定复杂的连接参数。

####  配置示例

`~/.ssh/config` 配置文件的参数是以 `Host` 块组织的。每个`Host` 块后接匹配规则用于匹配主机，匹配规则支持使用通配符。

```shell
Host Test-MC	# 指定单台主机，可直接ssh Test-MC登录
    User admin

Host 192.168.174.*	# 指定192.168.174.x网段的主机适用以下规则
    User test
    Port 22022
	
Host *-Product
    Port 8822
    HostName example.com
    IdentifyFile ~/id_rsa_upgrade	# 该规则指定该ssh私钥

Host *			# 表示匹配所有主机
    Port 7722
    IdentifyFile ~/id_rsa_common
```

规则的匹配顺序是从上至下进行匹配的，若有多条规则被匹配，后面规则不覆盖前面规则，但会追加未配置的规则。

如上面的配置，执行命令`ssh Test-MC` 登录，等同执行

```shell
$ ssh admin@Test-MC -p 7722 -i ~/id_rsa_common
```

#### 相关示例

- __端口映射__

设置远程主机的3306端口映射到本地的33006端口

```
Host db
	HostName db.example.com
	LocalForward 33006 localhost:3306
```

当连接远程主机时，会将远程主机的3306端口映射到本地的33006端口。

```shell
$ ssh db
$ mysql -uroot -pxxx -h localhost -p 33006
```

- 代理登录

若需要登录某跳板机才可登录其它主机。可以如下设置：

```
Host gateway
    HostName proxy.example.com
    User root
Host db
    HostName db.internal.example.com              # 目标服务器地址
    User root                                     # 用户名
    # IdentityFile ~/.ssh/id_ecdsa                # 认证文件
    ProxyCommand ssh gateway netcat -q 600 %h %p  # 代理命令
```

