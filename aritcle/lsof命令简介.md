### lsof 简介

__`lsof`__（list open files）是一个列出当前系统打开文件的工具。在linux环境中一切皆文件，通过文件不仅仅可以访问常规数据，还可以访问网络连接和硬件。所以如传输控制协议 (TCP) 和用户数据报协议 (UDP) 套接字等，系统在后台都为该应用程序分配了一个文件描述符，无论这个文件的本质如何，该文件描述符为应用程序与基础操作系统之间的交互提供了通用接口。因为应用程序打开文件的描述符列表提供了大量关于这个应用程序本身的信息，因此通过lsof工具能够查看这个列表对系统监测以及排错将是很有帮助的。

 ### lsof 命令解析

```
COMMAND    PID  TID    USER   FD      TYPE             DEVICE  SIZE/OFF       NODE NAME
systemd      1         root  cwd       DIR              253,0       224         64 /
systemd      1         root  rtd       DIR              253,0       224         64 /
systemd      1         root  txt       REG              253,0   1482128   67418805 /usr/lib/systemd/systemd
systemd      1         root  mem       REG              253,0     20040   33619254 /usr/lib64/libuuid.so.1.3.0
...
```
__`lsof`__ 输出各列信息的意义如下：
- COMMAND: 进程的名称 
- PID: 进程标识符
- USER: 进程所有者
- FD: 文件描述符，应用程序通过文件描述符识别该文件。每个进程都有自己的文件描述符表，因此FD可能会重名
- TYPE: 文件类型
- DEVICE: 指定磁盘的名称
- SIZE: 文件的大小
- NODE: 索引节点（文件在磁盘上的标识）
- NAME: 打开文件的确切名称

### 查看进程打开文件
- ##### 列出占用文件的进程 
```
$ sudo lsof /home/mysql/
COMMAND   PID  USER   FD   TYPE DEVICE SIZE/OFF   NODE NAME
bash    18522 mysql  cwd    DIR  253,0     4096 655378 /home/mysql
sudo    18544  root  cwd    DIR  253,0     4096 655378 /home/mysql
lsof    18545  root  cwd    DIR  253,0     4096 655378 /home/mysql
lsof    18546  root  cwd    DIR  253,0     4096 655378 /home/mysql
```
可配合 **`+d`** 选项，表示`/home/mysql`目录及目录下的文件，不包括子目录
```
$ sudo lsof /home/mysql/ +d
```

- ##### 列出pid打开的文件

`-p`选项，列出指定pid打开的文件，如果要多个，则用逗号隔开 
```
$ sudo lsof -p 16273
COMMAND   PID USER   FD   TYPE DEVICE SIZE/OFF    NODE NAME
nano    16273 root  cwd    DIR  253,0     4096 1179649 /root
```

- ##### 列出指定用户打开的文件

`-u`选项，列出指定用户打开的文件
```
$ sudo lsof -u test1
COMMAND   PID  USER   FD   TYPE DEVICE SIZE/OFF    NODE NAME
bash    16335 test1  cwd    DIR  253,0     4096 1966556 /home/test1
```

- ##### 列出指定进程名打开的文件

**`-c`** 选项，列出指定进程名打开的文件
```
sudo lsof -c java   # 显示command列中以java的所有打开的文件

sudo lsof -c java -c sh  # 显示command列中以java开头或以sh开头的打开的文件

sudo lsof -c ^java   # 显示command列中所有不以java开头的打开的文件
```

### 查看网络连接情况
**`-i`** 选项，可以列出网络连接情况。语法格式如下：
```
lsof -i[46] [protocol] [@hostname|hostaddr] [:service|prot]

参数解析：
4/6：IPv4或IPv6
protocol：TCP或UDP
hostname：主机名
hostaddr：ip地址
service：/etc/service中服务名，可写多个
port：端口号，可写多个
```
#### 示例：

**1. 查看IPv4网络情况**
```
$ lsof -i 4
COMMAND     PID      USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
rpcbind    1599       rpc    6u  IPv4  10168      0t0  UDP *:sunrpc 
rpcbind    1599       rpc    7u  IPv4  10169      0t0  UDP *:926 
...
```

**2. 查看指定IP**

**`-n`** 表示不进行IP域名反查，直接显示ip地址
```
$ lsof -i @10.0.137.144 -n
COMMAND   PID      USER   FD   TYPE   DEVICE SIZE/OFF NODE NAME
sshd    16734      root    3u  IPv4 14692046      0t0  TCP 10.0.137.144:ssh->10.0.2.136:49614 (ESTABLISHED)
sshd    16736      test    3u  IPv4 14692046      0t0  TCP 10.0.137.144:ssh->10.0.2.136:49614 (ESTABLISHED)
```

**3. 查看指定IP及端口**

```
$ sudo lsof -i @10.0.137.144:22 -n
COMMAND   PID      USER   FD   TYPE   DEVICE SIZE/OFF NODE NAME
sshd    16734      root    3u  IPv4 14692046      0t0  TCP 10.0.137.144:ssh->10.0.2.136:49614 (ESTABLISHED)
sshd    16736      test    3u  IPv4 14692046      0t0  TCP 10.0.137.144:ssh->10.0.2.136:49614 (ESTABLISHED)
```

**4. 查看指定端口**
```
$ lsof -i :22 -n
COMMAND   PID      USER   FD   TYPE   DEVICE SIZE/OFF NODE NAME
sshd     1095      root    3u  IPv4    14032      0t0  TCP *:ssh (LISTEN)
sshd     1095      root    4u  IPv6    14034      0t0  TCP *:ssh (LISTEN)
sshd    16734      root    3u  IPv4 14692046      0t0  TCP 10.0.137.144:ssh->10.0.2.136:49614 (ESTABLISHED)
sshd    16736      test    3u  IPv4 14692046      0t0  TCP 10.0.137.144:ssh->10.0.2.136:49614 (ESTABLISHED)
```

**5. 查看指定协议**
```
$ sudo lsof -i UDP
COMMAND    PID USER   FD   TYPE   DEVICE SIZE/OFF NODE NAME
rsyslogd 17935 root    3u  IPv4 11066422      0t0  UDP *:40296 
```

