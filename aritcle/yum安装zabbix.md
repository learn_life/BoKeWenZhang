环境准备

```
service iptables stop
setenfore 0
```

安装MySQL

```
rpm -ivh http://dev.mysql.com/get/mysql-community-release-el6-5.noarch.rpm
```

```
yum install mysql-server -y  
```

安装zabbix

```
rpm -ihv http://mirrors.aliyun.com/zabbix/zabbix/3.0/rhel/6/x86_64/zabbix-release-3.0-1.el6.noarch.rpm
```

```
yum install httpd zabbix zabbix-server zabbix-web zabbix-server-mysql zabbix-web-mysql -y
```

安装PHP

```
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
rpm -ivh http://mirror.webtatic.com/yum/el6/latest.rpm
```

```
yum install php70w php70w-mysql php70w-common php70w-gd php70w-mbstring php70w-mcrypt php70w-devel php70w-xml php70w-bcmath -y
```

MySQL配置

```shell
$ cp /usr/share/mysql/my-default.cnf /etc/my.cnf
$ /etc/init.d/mysqld start
$ mysql
mysql> create database zabbix character set utf8 collate utf8_bin;
mysql> grant all on zabbix.* to zabbix@'localhost' identified by '123456';
mysql> flush privileges;
mysql> exit
```

导入zabbix数据文件

```
cd /usr/share/doc/zabbix-server-mysql-3.0.3
zcat create.sql.gz |mysql -uzabbix -p123456 zabbix
```

修改php配置文件

```
$ vim /etc/php.ini
post_max_size = 16M
max_execution_time = 300
max_input_time = 300
date.timezone = Asia/shanghai
```

修改zabbix_server配置文件

```
$ vim /etc/zabbix/zabbix_server.conf
修改数据库地址、数据库名、 用户、密码分别对应:
DBHost=
DBName=
DBUser=
DBPassword=
DBSocket=/var/lib/mysql/mysql.sock
```

文件授权

```
cp -R /usr/share/zabbix/ /var/www/html/
chmod -R 755 /etc/zabbix/web
chown -R apache.apache /etc/zabbix/web 
```

启动服务

```
service mysqld restart
service zabbix-server restart
service httpd restart
```

