#### 数组简介

**bash** 只提供一维数组，并且没有限定数组的大小。类似与C语言，数组元素的下标由0开始编号。获取数组中的元素要利用下标。下标可以是整数或算术表达式，其值应大于或等于0。用户可以使用赋值语句对数组变量赋值。

#### 数组赋值

- 下标赋值
```shell
$ students[0]=Jack
$ students[1]=Alex
$ students[2]=Amy
```
也可以使用`declare`显式声明一个数组：
```
$ declare -a 数组名
```
- 直接赋值
```shell
$ students=(Jack Alex Amy)
或
$ declare -a studentds=(Jack Alex Amy)
```

- 命令赋值

命令的输出格式如下
```shell
$ ls
Desktop   Downloads  Pictures  Templates  virtualenv  
$ arr=($(ls))
```

- 字典赋值

可以通过`declare -A`命令声明字典
```shell
$ declare -A dict=([key1]=val1 [key2]=val2)
```

#### 访问数组

```
创建数组
$ students=(Jack Alex Amy)
```

- 通过下标访问

```shell
$ echo ${students[0]}
Jack
$ echo ${students[1]}
Alex
$ echo ${students[2]}
Amy
```

- 列出所有元素
```shell
$ echo ${students[@]}
Jack Alex Amy
或
$ echo ${students[*]}
Jack Alex Amy
```
__`@`__ 符号与 __`*`__ 符号均可以列出所有元素 

#### 数组的其它操作

- __获取数组长度__

```shell
$ echo ${#students[@]}
3
```

- __打印数组下标__ 

```shell
$ echo ${!students[@]}
0 1 2
```
也可以打印字典的key 值
```shell
$ declare -A dict=([key1]=val1 [key2]=val2)
$ echo ${!dict[@]}
key2 key1
```

- __删除数组__

```shell
$ unset 数组名
```
