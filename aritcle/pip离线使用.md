#### pip 相关操作简介

##### pip 常用命令

导出当前python环境已安装的软件包的名称及版本信息

```python
pip freeze > requirements.txt 
```

下载`requirement.txt` 中的软件包到本地（不安装）

```python
pip download -r requirements.txt
```

安装下载到本地的软件包

```
pip install --no-index --find-links=安装包路径 软件包名
```

##### 设置国内pip 源

```shell
mkdir ~/.pip
vim ~/.pip/pip.conf
[global]
index-url=https://pypi.douban.com/simple
[install]
trusted-host=pypi.douban.com
```

__国内pip源地址__


阿里云 <http://mirrors.aliyun.com/pypi/simple/>  

中国科技大学 <https://pypi.mirrors.ustc.edu.cn/simple/> 

豆瓣 <http://pypi.douban.com/simple>  

Python官方 <https://pypi.python.org/simple/>  

中国科学院 <http://pypi.mirrors.opencas.cn/simple/> 

 清华大学 <https://pypi.tuna.tsinghua.edu.cn/simple/> 

