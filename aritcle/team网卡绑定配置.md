### team网卡绑定

**实验环境**

- 系统：CentOS 7
- 网卡：`ens33`、`ens37` , 其中`ens33` 网卡优先使用。
- team0 : team配置IP地址为192.168.174.223
- 配置`activebackup`模式

**team网卡配置文件**

新建`/etc/sysconfig/network-scripts/ifcfg-team0` 网卡配置文件

```shell
DEVICE=team0
DEVICETYPE=Team			
ONBOOT=yes
BOOTPROTO=none
IPADDR=192.168.174.223
NETMASK=255.255.255.0
GATEWAY=192.168.174.2
DEFROUTE=yes
IPV4_FALLURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_PEERDNS=yes
IPV6_PEERROUTES=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
TEAM_CONFIG="{\"runner\": {\"name\": \"activebackup\"}}"	
```

其中，`DEVICE=team0` 配置设备名为team0 ， `DEVICETYPE=Team` 指定设备类型为team，`TEAM_CONFIG` 配置绑定模式。

**网卡文件配置**

备份原先的网卡配置文件

进行绑定配置

- ifcfg-ens33

  ```shell
  DEVICE=ens33
  ONBOOT=yes
  TEAM_MASTER=team0
  DEVICETYPE=TeamPort
  TEAM_PORT_CONFIG="{\"prio\": 100}"		
  ```
  其中`TEAM_PORT_CONFIG="{\"prio\": 100}"`，数值越大越优先

- ifcfg-ens37

  ```shell
  DEVICE=ens37
  ONBOOT=yes
  TEAM_MASTER=team0
  DEVICETYPE=TeamPort
  TEAM_PORT_CONFIG="{\"prio\": -10}"		
  ```

#### 配置完成执行命令：

**重启网络**

```shell
service network restart
```

**查看team网卡绑定状态**

```shell
[root@localhost ~]# teamdctl team0 state
setup:
  runner: activebackup
ports:
  ens33
    link watches:
      link summary: up
      instance[link_watch_0]:
        name: ethtool
        link: up
        down count: 0
  ens37
    link watches:
      link summary: up
      instance[link_watch_0]:
        name: ethtool
        link: up
        down count: 0
runner:
  active port: ens33
```

​	可以看到，team网卡绑定已生效，绑定为active-backup模式，且当前`ens33`网卡处于active状态。



