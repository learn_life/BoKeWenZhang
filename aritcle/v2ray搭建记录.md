### V2ray部署

直接使用脚本部署：

```bash
wget https://install.direct/go.sh
bash go.sh
```

重启服务

```bash
systemctl restart v2ray
```

### 开启加速

BBR的安装同样使用脚本，而且不需要任何配置。

```bash
 wget -N --no-check-certificate "https://raw.githubusercontent.com/chiakge/Linux-NetSpeed/master/tcp.sh" && chmod +x tcp.sh && ./tcp.sh
```

先选择安装安装 BBR/BBR魔改版内核内核，安装完成后，在运行tcp.sh按需要选择加速，可以选择魔改版加速。

#### 参考链接

- https://toutyrater.github.io/advanced/mkcp.html
- https://steemit.com/cn/@syt/v2ray-qos