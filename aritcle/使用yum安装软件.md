## yum缓存的好处

​	利用`yum`缓存安装软件几点好处：

- 利用`yum`缓存，离线安装软件
- 复制缓存软件包以作备用
- 无需联网，加快`yum`安装软件速度

## 开启yum缓存

​	默认`yum`是关闭缓存功能的，可以在`/etc/yum.conf`中将`keepcache`选项改为`1`

```
keepcache=1
```

​	通过`-C`选项指定`yum`使用缓存安装软件，如安装mysql-server这个软件，输入：

```
yum -C install mysql-server
```

##使用yum安装软件

​	当有多台相同主机需要通过`yum`安装软件时，可以利用`yum`的缓存功能完成。 下面以安装mysql-server作示例

- 主机A:

  通过yum源安装MySQL 6.5

```
rpm -ivh http://dev.mysql.com/get/mysql-community-release-el6-5.noarch.rpm
yum install mysql-server -y  
```

​	主机A安装完成后，将`/var/cache/yum/`复制到主机B相同目录下。注意，这里使用新增了MySQL的`yum`仓库，所以也要复制`/etc/yum.repos.d/mysql*`文件复制到主机B的相同目录下； 因为安装MySQL时有包的签名机制，所以要将`/etc/pki/rpm-gpg`目录也复制到主机B。

```
scp -r /var/cache/yum/* username@hostB:/var/cache/yum/
scp /etc/yum.repos.d/mysql* username@hostB:/etc/yum.repos.d/
scp /etc/pki/rpm-gpg/* username@hostB:/etc/pki/rpm-gpg/
```

- 主机B

  使用`yum`缓存安装`mysql-server`

```
yum -C install mysql-server
```

###  总结

​	总之，使用`yum`缓存安装软件可归纳为下面几个步骤：

- 源主机开启`yum`缓存，在 `/etc/yum.conf`中修改`keepcache=1`；安装所有需要的软件完毕。

- 将以下目录/文件复制到目标主机
  - `/var/cache/yum` 		
  - `/etc/yum.repos.d/`       
  - `/etc/pki/rpm-gpg/`

- 目标主机安装软件

  ```shell
  yum -C install 软件名
  ```

  ​