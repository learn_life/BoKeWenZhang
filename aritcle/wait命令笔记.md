## wait 命令

`wait`命令常用于等待指定的进程执行完成后继续执行后续的任务。`wait`命令可以在shell实现多线程同步功能，等待作业时，要在作业标识号前加上符号"%"

### 语法

```shell
wait [pid|job number]
```

### wait 使用示例

- wait 等待所有子进程结束

  `wait`后没有参数，则表示等待所有子进程结束

  ```shell
  #!/bin/bash
  sleep 8 &
  slepp 4 &
  wait        # 等待8秒后退出
  ```

- wait 等待指定子进程结束

  ```shell
  #!/bin/bash
  sleep 8 &
  sleep 4 &
  wait $!     # $!表示上个子进程的进程号，wait等待一个子进程，等待4秒后退出  
  ```

- wait 获得后台进程退出时的返回值

  用“&”把进程放到后台后，可以通过`wait`获得进程的执行情况。

  ```shell
  $ $(ls /not_exist &); wait $!
  ls: 无法访问/not_exist: 没有那个文件或目录
  $ echo $?
  2
  ```

