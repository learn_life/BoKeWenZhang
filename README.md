# 博客文章
---
#### [ansible笔记.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/ansible%E7%AC%94%E8%AE%B0.md)
#### [git分支学习笔记.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/git%E5%88%86%E6%94%AF%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0.md)
#### [iptables学习笔记.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/iptables%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0.md)
#### [lsof命令简介.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/lsof%E5%91%BD%E4%BB%A4%E7%AE%80%E4%BB%8B.md)
#### [nc 命令学习笔记.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/nc%20%E5%91%BD%E4%BB%A4%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0.md)
#### [pip离线使用.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/pip%E7%A6%BB%E7%BA%BF%E4%BD%BF%E7%94%A8.md)
#### [shell数组学习笔记.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/shell%E6%95%B0%E7%BB%84%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0.md)
#### [ssh客户端配置文件.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/ssh%E5%AE%A2%E6%88%B7%E7%AB%AF%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6.md)
#### [team网卡绑定配置.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/team%E7%BD%91%E5%8D%A1%E7%BB%91%E5%AE%9A%E9%85%8D%E7%BD%AE.md)
#### [v2ray搭建记录.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/v2ray%E6%90%AD%E5%BB%BA%E8%AE%B0%E5%BD%95.md)
#### [wait命令笔记.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/wait%E5%91%BD%E4%BB%A4%E7%AC%94%E8%AE%B0.md)
#### [xshell 端口转发.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/xshell%20%E7%AB%AF%E5%8F%A3%E8%BD%AC%E5%8F%91.md)
#### [yum安装zabbix.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/yum%E5%AE%89%E8%A3%85zabbix.md)
#### [使用yum安装软件.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/%E4%BD%BF%E7%94%A8yum%E5%AE%89%E8%A3%85%E8%BD%AF%E4%BB%B6.md)
#### [安装git.md](https://gitee.com/learn_life/BoKeWenZhang/blob/master/aritcle/%E5%AE%89%E8%A3%85git.md)
